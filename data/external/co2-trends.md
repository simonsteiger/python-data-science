# CO2 Trends

Source: [aftp.cmdl.noaa.gov](ftp://aftp.cmdl.noaa.gov/products/trends/co2/co2_weekly_mlo.txt)

## Read in Python

```python
data_url = "ftp://aftp.cmdl.noaa.gov/products/trends/co2/co2_weekly_mlo.txt"
co2_data = pd.read_csv(
    data_url,
    sep=r"\s+",
    comment="#",
    na_values="-999.99",
    names=[
        "year",
        "month",
        "day",
        "decimal",
        "ppm",
        "days",
        "1_yr_ago",
        "10_yr_ago",
        "since_1800",
    ],
)
```
