# Heart Disease

> Janosi,Andras, Steinbrunn,William, Pfisterer,Matthias, Detrano,Robert.
> 1988. Heart Disease. UCI Machine Learning Repository.

Source: [UC Irvine Machine Learning Repository](https://archive-beta.ics.uci.edu/dataset/45/heart+disease)  
License: [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/legalcode)

## Changes

- Original file `processed.cleveland.data`
- Add headers from source metadata
- Rename to `heart-disease.csv`
