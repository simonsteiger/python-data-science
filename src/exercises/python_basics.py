# %%
# Types

# Boolean (bool)
True, False

# Integer (int)
15

# Range (range)
range(1, 10, 2)

# Float (float)
2.66666

# Complex Number (complex)
1.5 + 3.2j

# String (str)
"Text", "Text"

# List
[1, 2, 3]

# Tuple
(1, 2, 3)

# Dictionary
{"a": 1, "b": 2, "c": 3}

# Set
{"a", "b", "c"}

# Absence of value
None

# %%
# Variables and Assignment (=)
a = 1
b = 4

c, d = 13, 14

# %%
# Mathematical Operations (+,-,*,**,/,//)
1 + 1
a - 2

b / c
b // 3  # Floor division

c * d
c**2  # Power

# %%
# Comparisons (==,!=,<,<=,>,>=)
a == 1  # True
a != 1  # False

a < 2  # True
a <= 2  # True

a > 2  # False
a >= 2  # False

# %%
# Boolean Operators (and/or/not)
a == 1 and not (b < 3 or b > 3)

# %%
# String Expressions (+,*)
"My name is " + "Peter"

"A" * 5  # "AAAAA"

# %%
# List Expressions (+,*)
["one", "list"] + ["two", "lists"]

["A"] * 5

# %%
# Conditional Execution (if/elif/else)
if a == b:
    print("a = b")
elif a > b:
    print("a > b")
else:
    print("a < b")

# %%
# For Loops (for)
for i in range(10):
    print(i)

# %%
# While Loops (while)
j = 0
while j < 10:
    print(j)
    j += 1  # j = j + 1

# %%
# List Comprehensions [...]
lc = [i for i in range(5)]

# Same as
ll = []
for i in range(5):
    ll.append(i)

lc == ll

# %%
# Dictionary Comprehensions {...}
keys = ["A", "B", "C"]
values = [1, 2, 3]

dc = {k: j for k, j in zip(keys, values)}

# Same as

dl = {}
for k, j in zip(keys, values):
    dl[k] = j

dc == dl

# %%
# Functions (def)
def my_function(a, b=2):
    return a**b


my_function(2)
my_function(2, 3)

# %%
# Anonymous Functions (lambda)
my_afunc = lambda x: x + 1

my_afunc(1)

# %%
# Import Packages (import)
import pandas as pd

pd.DataFrame()

# %%
# List Indexing
my_list = [1, 2, 3, 4, 5, 6, 7]

len(my_list)

my_list[0]
my_list[3]

my_list[-1]
my_list[-2]

my_list[1:3]
my_list[1:6:2]

# %%
# Dictionary Indexing
my_dict = {"A": 1, "B": 2, "C": 3}

len(my_dict)

my_dict["A"]
my_dict["C"]
