# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf

from src.config import DATA_PATH

# %%
patients = pd.read_csv(DATA_PATH / "generated/patients.csv", index_col="patient_id")

# %%
patients

# %%
patients.info()

# %%
patients.describe()

# %%
patients.hist()

# %%
fig, axs = plt.subplots(1, 3, figsize=(15, 4), constrained_layout=True)

patients.plot.scatter("age", "weight", ax=axs[0])
patients.plot.scatter("age", "systolic_bp", ax=axs[1])
patients.plot.scatter("weight", "systolic_bp", ax=axs[2])

# %%
ols_fit = smf.ols("weight ~ age", data=patients).fit()
ols_fit.summary()

age_weight_ax = patients.plot.scatter("age", "weight")

age_x = np.linspace(20, 80, 100)
age_weight_ax.plot(
    age_x, ols_fit.params["Intercept"] + ols_fit.params["age"] * age_x, color="red"
)

# %%
glm_fit = smf.glm("systolic_bp ~ age + weight", data=patients).fit()
glm_fit.summary()

# %%
therapies = pd.read_csv(DATA_PATH / "generated/therapies.csv", index_col="therapy_id")

# %%
therapies

# %%
therapies.info()

# %%
therapies.isna().sum()

# %%
therapies["therapy"].value_counts()

# %%
therapies.groupby("patient_id").size()

# %%
therapies.groupby("patient_id").size().scatter()

# %%
therapies.groupby("patient_id").size().value_counts()

# %%
therapies.groupby("patient_id").size().value_counts().plot.bar()

# %%
therapy_patients = therapies.join(patients, on="patient_id")

# %%
therapy_patients.assign(
    age_c=lambda x: pd.cut(x["age"], range(0, 101, 10), include_lowest=True)
)
