# %%
import numpy as np
import pandas as pd

from src.config import DATA_PATH

rng = np.random.RandomState(0)

# %%
N_PATIENTS = 1000
START_DATE = pd.to_datetime("2000-01-01")
STOP_DATE = pd.to_datetime("2022-12-31")
THERAPIES = ["A", "B", "C"]

# %%
@np.vectorize
def generate_therapy_episodes(start_date):
    therapy_episodes = []
    prev_stop = start_date

    while prev_stop < STOP_DATE:
        offset = pd.to_timedelta(rng.randint(-30, 366), "days")
        duration = pd.to_timedelta(rng.randint(1, 365 * 5), "days")

        start = prev_stop + offset
        stop = start + duration

        if stop <= STOP_DATE:
            therapy_episodes.append([start, stop])
        else:
            therapy_episodes.append([start, None])

        prev_stop = stop

    return therapy_episodes


# %%
patients = (
    pd.DataFrame(
        dict(
            age=rng.normal(50, 10, N_PATIENTS)
            .clip(min=18, max=110)
            .round()
            .astype(int),
            diagnosis_date=pd.Series(
                START_DATE + (STOP_DATE - START_DATE) * rng.random(N_PATIENTS)
            ).round("d"),
        )
    )
    .assign(
        weight=lambda x: rng.normal(50 + 0.3 * x["age"], 10).round(1),
        systolic_bp=lambda x: rng.normal(
            90 + 0.5 * x["age"] + 0.2 * x["weight"], 10
        ).round(1),
    )
    .rename_axis(index="patient_id")
)

patients.to_csv(DATA_PATH / "generated/patients.csv")

# %%
therapies = (
    pd.Series(
        generate_therapy_episodes(patients["diagnosis_date"]), index=patients.index
    )
    .explode()
    .pipe(lambda x: pd.DataFrame(x.to_list(), index=x.index, columns=["start", "stop"]))
    .reset_index()
    .rename_axis(index="therapy_id")
    .assign(therapy=lambda x: rng.choice(THERAPIES, x.shape[0]))
)

therapies.to_csv(DATA_PATH / "generated/therapies.csv")
