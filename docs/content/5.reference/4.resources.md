# Resources

There are many resources for taking your Python skills to the next level.
Below are some websites and books that can help you on your way.
Apart from these, the answers to most questions are just a web search away thanks to Pythons large and active community.

## :globe_with_meridians: Websites

::list{type="info"}
- https://www.python.org/
- https://www.learnpython.org/
- https://www.pythonforbeginners.com/
- https://python.swaroopch.com
- https://github.com/vinta/awesome-python
::

## :books: Books

::list{type="info"}
- https://wesmckinney.com/book/
::
