export default defineNuxtConfig({
  extends: '@nuxt-themes/docus',
  telemetry: false,
  components: [{
    path: '~/components',
    global: true
  }],
    content: {
      highlight: {
        theme: {
          default: 'github-light',
          dark: 'github-dark',
        },
        preload: ["python", "bash"]
      }
    }
})
