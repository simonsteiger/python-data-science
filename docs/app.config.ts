export default defineAppConfig({
  docus: {
    title: 'Python / Data Science',
    description: 'An introduction to using Python for Data Science.',
    url: 'https://pythondatascience.dev/',
    header: {
      logo: false,
    },
  }
})
